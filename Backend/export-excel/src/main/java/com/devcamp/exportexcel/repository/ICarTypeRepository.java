package com.devcamp.exportexcel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.exportexcel.model.CarType;

public interface ICarTypeRepository extends JpaRepository<CarType, Long> {

}
